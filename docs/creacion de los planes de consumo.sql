--------------------------------------------------------
-- Archivo creado  - domingo-marzo-17-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table PLAN_DE_CONSUMO
--------------------------------------------------------

  CREATE TABLE "ISIS2304C311910"."PLAN_DE_CONSUMO" 
   (	"ID" NUMBER, 
	"NOMBRE" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  TABLESPACE "TBSPROD" ;
--------------------------------------------------------
--  DDL for Index PLAN_DE_CONSUMO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ISIS2304C311910"."PLAN_DE_CONSUMO_PK" ON "ISIS2304C311910"."PLAN_DE_CONSUMO" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 NOLOGGING 
  TABLESPACE "TBSPROD" ;
--------------------------------------------------------
--  Constraints for Table PLAN_DE_CONSUMO
--------------------------------------------------------

  ALTER TABLE "ISIS2304C311910"."PLAN_DE_CONSUMO" MODIFY ("NOMBRE" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."PLAN_DE_CONSUMO" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."PLAN_DE_CONSUMO" ADD CONSTRAINT "PLAN_DE_CONSUMO_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 NOLOGGING 
  TABLESPACE "TBSPROD"  ENABLE;
