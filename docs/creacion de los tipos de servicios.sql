--------------------------------------------------------
-- Archivo creado  - domingo-marzo-17-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table TIPO_SERVICIOS
--------------------------------------------------------

  CREATE TABLE "ISIS2304C311910"."TIPO_SERVICIOS" 
   (	"ID_SERVICIO" NUMBER, 
	"NOMBRE" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  TABLESPACE "TBSPROD" ;
--------------------------------------------------------
--  DDL for Index SERVICIOS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ISIS2304C311910"."SERVICIOS_PK" ON "ISIS2304C311910"."TIPO_SERVICIOS" ("ID_SERVICIO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOLOGGING 
  TABLESPACE "TBSPROD" ;
--------------------------------------------------------
--  Constraints for Table TIPO_SERVICIOS
--------------------------------------------------------

  ALTER TABLE "ISIS2304C311910"."TIPO_SERVICIOS" MODIFY ("NOMBRE" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."TIPO_SERVICIOS" MODIFY ("ID_SERVICIO" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."TIPO_SERVICIOS" ADD CONSTRAINT "SERVICIOS_PK" PRIMARY KEY ("ID_SERVICIO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOLOGGING 
  TABLESPACE "TBSPROD"  ENABLE;
