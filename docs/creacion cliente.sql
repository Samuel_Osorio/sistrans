--------------------------------------------------------
-- Archivo creado  - domingo-marzo-17-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table CLIENTE
--------------------------------------------------------

  CREATE TABLE "ISIS2304C311910"."CLIENTE" 
   (	"NUM_DOCUMENTO" NUMBER, 
	"TIPO_DOCUMENTO" VARCHAR2(20 BYTE), 
	"CORREO" VARCHAR2(20 BYTE), 
	"NOMBRE" VARCHAR2(20 BYTE), 
	"ID_HABITACION" NUMBER, 
	"ID_HOTEL" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  TABLESPACE "TBSPROD" ;
--------------------------------------------------------
--  DDL for Index CLIENTE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ISIS2304C311910"."CLIENTE_PK" ON "ISIS2304C311910"."CLIENTE" ("NUM_DOCUMENTO", "TIPO_DOCUMENTO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 NOLOGGING 
  TABLESPACE "TBSPROD" ;
--------------------------------------------------------
--  Constraints for Table CLIENTE
--------------------------------------------------------

  ALTER TABLE "ISIS2304C311910"."CLIENTE" MODIFY ("NOMBRE" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."CLIENTE" MODIFY ("CORREO" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."CLIENTE" MODIFY ("TIPO_DOCUMENTO" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."CLIENTE" MODIFY ("NUM_DOCUMENTO" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."CLIENTE" ADD CONSTRAINT "CLIENTE_PK" PRIMARY KEY ("NUM_DOCUMENTO", "TIPO_DOCUMENTO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 NOLOGGING 
  TABLESPACE "TBSPROD"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table CLIENTE
--------------------------------------------------------

  ALTER TABLE "ISIS2304C311910"."CLIENTE" ADD CONSTRAINT "CLIENTE_FK_HABITA" FOREIGN KEY ("ID_HABITACION", "ID_HOTEL")
	  REFERENCES "ISIS2304C311910"."HABITACIONES" ("ID", "ID_HOTEL") ON DELETE SET NULL ENABLE;
