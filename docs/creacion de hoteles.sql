--------------------------------------------------------
-- Archivo creado  - domingo-marzo-17-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table HOTEL
--------------------------------------------------------

  CREATE TABLE "ISIS2304C311910"."HOTEL" 
   (	"ID" NUMBER, 
	"NOMBRE" VARCHAR2(20 BYTE), 
	"PAIS" VARCHAR2(20 BYTE), 
	"NUM_HABITACIONES" NUMBER, 
	"DIRECCION" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TBSPROD" ;
--------------------------------------------------------
--  DDL for Index HOTEL_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ISIS2304C311910"."HOTEL_PK" ON "ISIS2304C311910"."HOTEL" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOLOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TBSPROD" ;
--------------------------------------------------------
--  Constraints for Table HOTEL
--------------------------------------------------------

  ALTER TABLE "ISIS2304C311910"."HOTEL" MODIFY ("DIRECCION" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."HOTEL" MODIFY ("NUM_HABITACIONES" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."HOTEL" MODIFY ("PAIS" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."HOTEL" MODIFY ("NOMBRE" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."HOTEL" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."HOTEL" ADD CONSTRAINT "HOTEL_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOLOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "TBSPROD"  ENABLE;
  ALTER TABLE "ISIS2304C311910"."HOTEL" ADD CONSTRAINT "HOTEL_NO_HAB_NEG_CK" CHECK (NUM_HABITACIONES>0) ENABLE;
