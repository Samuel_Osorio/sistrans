--------------------------------------------------------
-- Archivo creado  - domingo-marzo-17-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table INVENTARIO
--------------------------------------------------------

  CREATE TABLE "ISIS2304C311910"."INVENTARIO" 
   (	"ID" NUMBER, 
	"ID_HOTEL" NUMBER, 
	"ID_TIPO" NUMBER, 
	"ID_SERVICIO" NUMBER, 
	"NOMBRE" VARCHAR2(20 BYTE), 
	"PRECIO" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  TABLESPACE "TBSPROD" ;
--------------------------------------------------------
--  DDL for Index INVENTARIO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "ISIS2304C311910"."INVENTARIO_PK" ON "ISIS2304C311910"."INVENTARIO" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 NOLOGGING 
  TABLESPACE "TBSPROD" ;
--------------------------------------------------------
--  Constraints for Table INVENTARIO
--------------------------------------------------------

  ALTER TABLE "ISIS2304C311910"."INVENTARIO" MODIFY ("NOMBRE" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."INVENTARIO" MODIFY ("ID_HOTEL" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."INVENTARIO" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."INVENTARIO" ADD CONSTRAINT "INVENTARIO_PRICE_NEG" CHECK (PRECIO>0) ENABLE;
  ALTER TABLE "ISIS2304C311910"."INVENTARIO" ADD CONSTRAINT "INVENTARIO_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 NOLOGGING 
  TABLESPACE "TBSPROD"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table INVENTARIO
--------------------------------------------------------

  ALTER TABLE "ISIS2304C311910"."INVENTARIO" ADD CONSTRAINT "INVENTARIO_FK1" FOREIGN KEY ("ID_HOTEL", "ID_SERVICIO", "ID_TIPO")
	  REFERENCES "ISIS2304C311910"."HOTEL_TIPOS_SERVICIOS" ("ID_HOTEL", "ID_SERVICIO", "ID_TIPO") ENABLE;
