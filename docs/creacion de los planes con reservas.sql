--------------------------------------------------------
-- Archivo creado  - domingo-marzo-17-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table RESERVAS_PLANES
--------------------------------------------------------

  CREATE TABLE "ISIS2304C311910"."RESERVAS_PLANES" 
   (	"ID_RESERVA" VARCHAR2(20 BYTE), 
	"ID_PLAN" NUMBER, 
	"FECHA_INICIO" DATE, 
	"FECHA_FIN" DATE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  TABLESPACE "TBSPROD" ;
--------------------------------------------------------
--  Constraints for Table RESERVAS_PLANES
--------------------------------------------------------

  ALTER TABLE "ISIS2304C311910"."RESERVAS_PLANES" MODIFY ("FECHA_FIN" NOT NULL ENABLE);
  ALTER TABLE "ISIS2304C311910"."RESERVAS_PLANES" MODIFY ("FECHA_INICIO" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table RESERVAS_PLANES
--------------------------------------------------------

  ALTER TABLE "ISIS2304C311910"."RESERVAS_PLANES" ADD CONSTRAINT "RESERVAS_PLANES_PLAN_FK" FOREIGN KEY ("ID_PLAN")
	  REFERENCES "ISIS2304C311910"."PLAN_DE_CONSUMO" ("ID") ENABLE;
  ALTER TABLE "ISIS2304C311910"."RESERVAS_PLANES" ADD CONSTRAINT "RESERVAS_PLANES_RES_FK" FOREIGN KEY ("ID_RESERVA")
	  REFERENCES "ISIS2304C311910"."RESERVA" ("ID") ENABLE;
