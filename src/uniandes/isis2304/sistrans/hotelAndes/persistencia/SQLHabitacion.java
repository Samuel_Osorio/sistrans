package uniandes.isis2304.sistrans.hotelAndes.persistencia;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.sistrans.hotelAndes.negocio.Habitacion;
import uniandes.isis2304.sistrans.hotelAndes.negocio.Reserva;
import uniandes.isis2304.sistrans.hotelAndes.negocio.Usuario;

public class SQLHabitacion {

	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	
	/**
	 * 
	 */
	private static final String SQL = PersistenciaHotelAndes.SQL;
	
	

	

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * 
	 */
	private PersistenciaHotelAndes ph;
	
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor
	 * @param ph - El Manejador de persistencia de la aplicación
	 */
	public SQLHabitacion(PersistenciaHotelAndes ph) {

		this.ph = ph;
		
	}
	
	
	
	/**
	 * 
	 * @param pm
	 * @param id
	 * @param idHotel
	 * @param tipo
	 * @param reservada
	 * @param capacidad
	 * @param costo
	 * @param idReserva
	 * @return
	 */
	public long adicionarHabitacion (PersistenceManager pm, long id, long idHotel, String tipo, char reservada, int capacidad, int costo, String idReserva) 
	{
        Query q = pm.newQuery(SQL, "INSERT INTO " + ph.darTablaHabitacion() + "(id, idHotel, tipo, reservada, capacidad, costo, idReserva) values (?, ?, ?, ?, ?, ?, ?)");
        q.setParameters(id, idHotel, tipo, reservada, capacidad, costo, idReserva);
        return (long) q.executeUnique();            
	}
	
	
	/**
	 * 
	 * @param pm
	 * @param idBebida
	 * @return
	 */
	public long eliminarHabitacionPorIdHabitacionIdHotel (PersistenceManager pm, long id, long idHotel)
	{
        Query q = pm.newQuery(SQL, "DELETE FROM " + ph.darTablaHabitacion() + " WHERE ID = ? AND ID_HOTEL = ?");
        q.setParameters(id, idHotel);
        return (long) q.executeUnique();            
	}
	
	
	/**
	 * 
	 * @param pm
	 * @param idHotel
	 * @return
	 */
	public List<Habitacion> darHabitacionesPorIdHotel (PersistenceManager pm, long idHotel) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + ph.darTablaHabitacion () + " WHERE  ID_HOTEL = ?");
		q.setResultClass(Habitacion.class);
		q.setParameters(idHotel);
		return (List<Habitacion>) q.executeList();
	}
	
	
	/**
	 * 
	 * @param pm
	 * @param idHotel
	 * @return
	 */
	public List<Habitacion> darHabitacionesDisponiblesPorHotel (PersistenceManager pm, long idHotel) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + ph.darTablaHabitacion () + " WHERE  ID_HOTEL = ? AND RESERVADA = 'N'");
		q.setResultClass(Habitacion.class);
		q.setParameters(idHotel);
		return (List<Habitacion>) q.executeList();
	}
	
	/**
	 * 
	 * @param pm
	 * @param idHotel
	 * @return
	 */
	public List<Habitacion> darHabitacionesDisponiblesPorHotelDeCiertoTipo (PersistenceManager pm, long idHotel, String tipoHabitacion) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + ph.darTablaHabitacion () + " WHERE  ID_HOTEL = ? AND TIPO = ? AND RESERVADA = 'N' ");
		q.setResultClass(Habitacion.class);
		q.setParameters(idHotel, tipoHabitacion);
		return (List<Habitacion>) q.executeList();
	}
	
	/**
	 * 
	 * @param pm
	 * @param idHotel
	 * @return
	 */
	public List<Habitacion> darHabitacionesDisponiblesPorHotelDeCiertoTip(PersistenceManager pm, long idHotel, String tipoHabitacion) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + ph.darTablaHabitacion () + " WHERE  ID_HOTEL = ? AND TIPO = ? AND RESERVADA = 'N' ");
		q.setResultClass(Habitacion.class);
		q.setParameters(idHotel, tipoHabitacion);
		return (List<Habitacion>) q.executeList();
	}
	
	
	/**
	 * 
	 * @param pm
	 * @param idHabitacion
	 * @param idHotel
	 * @return
	 */
	public Habitacion darHabitacionPorId (PersistenceManager pm, long idHabitacion, long idHotel) 
	{
		Query q = pm.newQuery(ph.SQL, "SELECT * FROM " + ph.darTablaUsuario () + " WHERE ID = ? AND ID_HOTEL = ?");
		q.setResultClass(Habitacion.class);
		q.setParameters(idHabitacion, idHotel);
		return (Habitacion) q.executeUnique();
	}
	
	
	public List<Habitacion> darHabitacionesPorNumReserva (PersistenceManager pm, String idReserva) 
	{
		Query q = pm.newQuery(ph.SQL, "SELECT * FROM " + ph.darTablaUsuario () + " WHERE COD_RESERVA");
		q.setParameters(idReserva);
		return (List<Habitacion>) q.executeList();
	}
	
	
	/**
	 * 
	 * @param pm
	 * @param idHabitacion
	 * @param idHotel
	 * @param idReserva
	 */
	public void ocuparUnaHabitacion(PersistenceManager pm, long idHabitacion, long idHotel, String idReserva)
	{
		Query q = pm.newQuery(SQL, "UPDATE " + ph.darTablaHabitacion() + "SET RESERVADA = 'Y' AND COD_RESERVA = ?" );
		q.setParameters(idReserva);
		q.executeUnique();
	}
	
	
	/**
	 * 
	 * @param pm
	 * @param idHabitacion
	 * @param idHotel
	 * @param idReserva
	 */
	public void desocuparUnaHabitacion(PersistenceManager pm, long idHabitacion, long idHotel)
	{
		Query q = pm.newQuery(SQL, "UPDATE " + ph.darTablaHabitacion() + "SET RESERVADA = 'N' AND COD_RESERVA = NULL" );
		q.executeUnique();
	}
	
	public void sacarDeMantenimiento(PersistenceManager pm, long idMantenimiento)
	{
		Query q = pm.newQuery(SQL, "UPDATE " + ph.darTablaHabitacion() + "SET ID_MANTENIMIENTO = NULL WHERE ID_MANTENIMIENTO = ?");
		q.setParameters(idMantenimiento);
		q.executeUnique();
	}
	
	public List<Reserva> darReservaFechas(PersistenceManager pm, String fechaInicial, String fechaFinal){
		Query q = pm.newQuery(SQL, "SELECT * FROM " + ph.darTablaReserva() + " WHERE FECHA_ENTRADA <= ? AND FECHA_SALIDA >= ?");
		q.setParameters(fechaInicial, fechaFinal);
		return (List<Reserva>) q.executeList();
	}
	
	public void regitrarMantenimiento(PersistenceManager pm, List<Habitacion> habitaciones, String fechaInicial, String fechaFinal) {
		List<Reserva> reservas = darReservaFechas(pm, fechaInicial, fechaFinal);
		ArrayList<Habitacion> mover = new ArrayList<>();
		for(int i = 0; i<habitaciones.size(); i++) {
			for(int j = 0; j<reservas.size(); j++) {
				if(reservas.get(j).getIdReserva() == habitaciones.get(i).reserva.getIdReserva()) {
					mover.add(habitaciones.get(i));
				}
			}
		}
		
		for(int i = 0; i<mover.size(); i++) {
			List<Habitacion> x = darHabitacionesDisponiblesPorHotelDeCiertoTipo(pm, mover.get(i).hotel.getId(), mover.get(i).tipo_habitacion);

			if(x.isEmpty()) {
				String tipo = darNuevoTipoHabitacion(mover.get(i).tipo_habitacion);
				x = darHabitacionesDisponiblesPorHotelDeCiertoTipo(pm, mover.get(i).hotel.getId(), tipo);
				actualizarHabitaciones(pm, x.get(0).getId_habitacion(), mover.get(i).getId_habitacion());
			}
			else {
				actualizarHabitaciones(pm, x.get(0).getId_habitacion(), mover.get(i).getId_habitacion());
			}
		}
	}
	
	public String darNuevoTipoHabitacion(String habitacion) {
		if(habitacion.equals("SENCILLA"))
			return "DOBLE";
		else if(habitacion.equals("DOBLE"))
			return "SUITE FAMILIAR";
		else if(habitacion.equals("SUITE FAMILIAR"))
			return "SUITE PRESIDENCIAL";
		else
			return "SUITE FAMILIAR";
	}
	
	public void actualizarHabitaciones(PersistenceManager pm, long l, long m) {
		Query q = pm.newQuery(SQL, "UPDATE " + ph.darTablaFacturacion() + " SET ID_HABITACION = ? WHERE ID_HABITACION = ?");
		Query q2 = pm.newQuery(SQL, "UPDATE " + ph.darTablaHabitacion() + " SET ID = ? WHERE ID = ?") ;
		
		q2.setParameters(l, m);
		q.setParameters(l, m);
		q.executeUnique();
		q2.executeUnique();
	}
	
	
}
