package uniandes.isis2304.sistrans.hotelAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.sistrans.hotelAndes.negocio.Cliente;
import uniandes.isis2304.sistrans.hotelAndes.negocio.Habitacion;
import uniandes.isis2304.sistrans.hotelAndes.negocio.Usuario;


class SQLUsuario {
	
	public PersistenciaHotelAndes ph;
	
	/**
	 * 
	 */
	private SQLHabitacion accesoHabitacion;
	
	/**
	 * 
	 */
	private SQLCliente accesoCliente;
	
	/**
	 * 
	 */
	private SQLReserva accesoReservas;
	
	/**
	 * 
	 * @param ph
	 */
	public SQLUsuario(PersistenciaHotelAndes ph) {
		this.ph = ph;
	}
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un BEBEDOR a la base de datos de Parranderos
	 * @param pm - El manejador de persistencia
	 * @param idBebedor - El identificador del bebedor
	 * @param nombre - El nombre del bebedor
	 * @param ciudad - La ciudad del bebedor
	 * @param presupuesto - El presupuesto del bebedor (ALTO, MEDIO, BAJO)
	 * @return EL n�mero de tuplas insertadas
	 */
	public long adicionarUsuario (PersistenceManager pm, int numDoc, String tipoDocumento, String nombre, String correo, int idHotel, String tipo) 
	{
		Query q = pm.newQuery(ph.SQL, "INSERT INTO " + ph.darTablaUsuario() + "(NUM_DOCUMENTO, TIPO_DOCUMENTO, NOMBRE, CORREO, ID_HOTEL, TIPO) values (?, ?, ?, ?, ?, ?)");
		q.setParameters(numDoc, tipoDocumento, nombre, correo, idHotel, tipo);
		return (long) q.executeUnique();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar BEBEDORES de la base de datos de Parranderos, por su nombre
	 * @param pm - El manejador de persistencia
	 * @param nombre - El nombre del bebedor
	 * @return EL n�mero de tuplas eliminadas
	 */
	public long eliminarUsuarioPorNombre (PersistenceManager pm, String nombre)
	{
        Query q = pm.newQuery(ph.SQL, "DELETE FROM " + ph.darTablaUsuario () + " WHERE NOMBRE = ?");
        q.setParameters(nombre);
        return (long) q.executeUnique();            
	}

	/**
	 * Crea y ejecuta la sentencia SQL para eliminar UN BEBEDOR de la base de datos de Parranderos, por su identificador
	 * @param pm - El manejador de persistencia
	 * @param idBebedor - El identificador del bebeodor
	 * @return EL n�mero de tuplas eliminadas
	 */
	public long eliminarBebedorPorId (PersistenceManager pm, long idBebedor)
	{
        Query q = pm.newQuery(ph.SQL, "DELETE FROM " + ph.darTablaUsuario () + " WHERE NUM_DOCUMENTO = ?");
        q.setParameters(idBebedor);
        return (long) q.executeUnique();            
	}

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la informaci�n de UN BEBEDOR de la 
	 * base de datos de Parranderos, por su identificador
	 * @param pm - El manejador de persistencia
	 * @param idBebedor - El identificador del bebedor
	 * @return El objeto BEBEDOR que tiene el identificador dado
	 */
	public Usuario darUsuarioPorId (PersistenceManager pm, long idBebedor) 
	{
		Query q = pm.newQuery(ph.SQL, "SELECT * FROM " + ph.darTablaUsuario () + " WHERE NUM_DOCUMENTO = ?");
		q.setResultClass(Usuario.class);
		q.setParameters(idBebedor);
		return (Usuario) q.executeUnique();
	}
	
	
	
}
