package uniandes.isis2304.sistrans.hotelAndes.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLConsumo {
	PersistenciaHotelAndes ph;
	public SQLConsumo(PersistenciaHotelAndes ph) {
		this.ph = ph;
	}
	
	public long agregarReserva(PersistenceManager pm, int pId, String pInicio, String pFin, int pCantidad, char pCargo, int pProducto, int pHabitacion, int pHotel) {
		Query q = pm.newQuery(ph.SQL, "INSERT INTO " + ph.darTablaConsumo() + "(ID, ID_PRODUCTO, FECHA_INICIO, FECHA_FIN, CANTIDAD, CARGO_HABITACION, ID_HABITACION, ID_HOTEL) values (?, ?, ?, ?, ?, ?, ?, ?)");
		q.setParameters(pId, pProducto, pInicio, pFin, pCantidad, pCargo, pHabitacion, pHotel);
		return (long) q.executeUnique();
	}
}
