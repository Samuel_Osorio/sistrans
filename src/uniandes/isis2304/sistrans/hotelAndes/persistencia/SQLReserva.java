package uniandes.isis2304.sistrans.hotelAndes.persistencia;

import javax.jdo.PersistenceManager;

import javax.jdo.Query;

import uniandes.isis2304.sistrans.hotelAndes.negocio.Reserva;

public class SQLReserva {
	
	
	/**
	 * 
	 */
	private static final String SQL = PersistenciaHotelAndes.SQL;
	
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * 
	 */
	private PersistenciaHotelAndes ph;
	
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	
	/**
	 * Constructor
	 * @param ph - El Manejador de persistencia de la aplicación
	 */
	public SQLReserva(PersistenciaHotelAndes ph) {
		this.ph = ph;
	}
	
	
	public long darDocumentoClienteReservo(PersistenceManager pm, String idReserva)
	{
		Query p = pm.newQuery(ph.SQL, "SELECT RESERVA.NUM_DOC_CLIENTE FROM" + ph.darTablaReserva() + "WHERE ID = ?");
		p.setParameters(idReserva);
		return (long) p.executeUnique();
	}
	
	public String darTipoDocumentoClienteReservo(PersistenceManager pm, String idReserva)
	{
		Query p = pm.newQuery(ph.SQL, "SELECT RESERVA.TIPO_DOC_CLIENTE FROM" + ph.darTablaReserva() + "WHERE ID = ?");
		p.setParameters(idReserva);
		return (String) p.executeUnique();
	}
	
	public Reserva darReserva(PersistenceManager pm, String idReserva)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + ph.darTablaReserva() + " WHERE id = ?");
		q.setResultClass(Reserva.class);
		q.setParameters(idReserva);
		return (Reserva) q.executeUnique();
	}
	
	public long agregarReserva(PersistenceManager pm, String pId, String pEntrada, String pSalida, int pNum, String pTipo, int pDoc) {
		Query q = pm.newQuery(ph.SQL, "INSERT INTO " + ph.darTablaReserva() + "(ID, FECHA_ENTRADA, FECHA_SALIDA, NUM_PERSONAS, TIPO_DOC_CLIENTE, NUM_DOC_CLIENTE) values (?, ?, ?, ?, ?, ?)");
		q.setParameters(pId, pEntrada, pSalida, pNum, pTipo, pDoc);
		return (long) q.executeUnique();
	}
	
	public long[] eliminarReservasConvencion(PersistenceManager pm, String idCliente) {
		Query qi = pm.newQuery(SQL, "DELETE * FROM " + ph.darTablaReserva() + " WHERE NUM_DOC_CLIENTE = ?");
		qi.setParameters(idCliente);
		Query qf = pm.newQuery(SQL, "DELETE * FROM " + ph.darTablaReservaServicio() +" WHERE NUM_DOC_CLIENTE = ?");
		qf.setParameters(idCliente);
		
		long reserva = (long) qi.executeUnique();
		long servicios = (long) qf.executeUnique();
		
		return new long[] {reserva, servicios};
	}
}
