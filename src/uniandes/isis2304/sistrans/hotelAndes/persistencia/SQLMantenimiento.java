package uniandes.isis2304.sistrans.hotelAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.sistrans.hotelAndes.negocio.Mantenimiento;

public class SQLMantenimiento {
	

	/**
	 * 
	 */
	private static final String SQL = PersistenciaHotelAndes.SQL;

	/**
	 * 
	 */
	public PersistenciaHotelAndes ph;
	
	
	/**
	 * 
	 */
	public SQLMantenimiento(PersistenciaHotelAndes pp) {
		ph = pp;
	}
	
	
	public Mantenimiento darMantenimientoPorID(PersistenceManager pm, long id)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM" + ph.darTablaMantenimiento() + "WHERE ID = ?");
		q.setParameters(id);
		q.setClass(Mantenimiento.class);
		return (Mantenimiento) q.executeUnique();
	}
	
	public List<Mantenimiento> darMantenimientos(PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM" + ph.darTablaMantenimiento());
		q.setClass(Mantenimiento.class);
		return q.executeList();
	}
	
	public List<Mantenimiento> darMantenimientosExpirados(PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + ph.darTablaMantenimiento()+ "WHERE FECHA_FIN > " + "(SELECT TO_CHAR(SYSDATE, 'MM-DD-YYYY HH24:MI:SS') 'NOW' FROM DUAL)");
		return q.executeList();
	} 
}
