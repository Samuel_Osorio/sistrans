package uniandes.isis2304.sistrans.hotelAndes.persistencia;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.sistrans.hotelAndes.negocio.Reserva;
import uniandes.isis2304.sistrans.hotelAndes.negocio.ReservaServicio;

public class SQLReservaServicio {
	
	/**
	 * 
	 */
	private static final String SQL = PersistenciaHotelAndes.SQL;
	
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * 
	 */
	private PersistenciaHotelAndes ph;
	
	
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	
	/**
	 * Constructor
	 * @param ph - El Manejador de persistencia de la aplicación
	 */
	public SQLReservaServicio(PersistenciaHotelAndes ph) {
		this.ph = ph;
	}
	
	/**
	 * Crea y ejecuta la sentencia para agregar una reserva de un servicio
	 * @param pm
	 * @param pId
	 * @param idHotel
	 * @param idServicio
	 * @param idTipo
	 * @param idHab
	 * @param fechaInicial
	 * @param pDoc
	 * @return
	 */
	public long agregarReserva(PersistenceManager pm, long pId, long idHotel, long idServicio, long idTipo, long idHab, Date fechaInicial, Date pDoc) {
		Query q = pm.newQuery(ph.SQL, "INSERT INTO " + ph.darTablaReserva() + "(ID, ID_HOTEL, ID_SERVICIO, ID_TIPO, ID_HABITACION, FECHA_INICIAL, FECHA_FINAL) values (?, ?, ?, ?, ?, ?, ?)");
		q.setParameters(pId, idHotel, idServicio, idTipo, idHab, fechaInicial, pDoc);
		return (long) q.executeUnique();
	}
	
	/**
	 * Da la reserva de un servicio a partir de cierto id
	 * @param pm
	 * @param idReserva
	 * @return
	 */
	public Reserva darReserva(PersistenceManager pm, long idReserva)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + ph.darTablaReservaServicio() + " WHERE ID = ?");
		q.setResultClass(Reserva.class);
		q.setParameters(idReserva);
		return (Reserva) q.executeUnique();
	}
	
	
	/**
	 * 
	 * @param pm
	 * @param idReserva
	 * @return
	 */
	public long eliminarReservaPorID(PersistenceManager pm, long idReserva)
	{
        Query q = pm.newQuery(ph.SQL, "DELETE FROM " + ph.darTablaReservaServicio() + " WHERE ID = ?");
        q.setParameters(idReserva);
        return (long) q.executeUnique();            
	}
	
	public List<ReservaServicio> darReservaTiempo(PersistenceManager pm, String fechaInicial, String fechaFinal){
		Query q = pm.newQuery(SQL, "SELECT * FROM " + ph.darTablaReservaServicio() + " WHERE FECHA_INICIAL <= ? AND FECHA_FINAL >= ?");
		q.setParameters(fechaInicial, fechaFinal);
		return (List<ReservaServicio>) q.executeList();
	}
	
	public void mantenimientoServicios(PersistenceManager pm, String inicial, String fin, List<ReservaServicio> servicios, int servicioNuevo) {
		ArrayList<ReservaServicio> mover = new ArrayList<ReservaServicio>();
		List<ReservaServicio> fechas = darReservaTiempo(pm, inicial, fin);
		
		for(int i = 0; i<servicios.size(); i++) {
			for(int j = 0; j<fechas.size(); j++) {
				if(servicios.get(i).servicio == fechas.get(j).servicio)
					mover.add(servicios.get(i));
			}
		}
		
		for(int i = 0; i<mover.size(); i++) {
			actualizarServicios(pm, mover.get(i).servicio, servicioNuevo);
		}
	}
	
	public void actualizarServicios(PersistenceManager pm, long viejo, long nuevo) {
		Query q = pm.newQuery(SQL, "UPDATE " + ph.darTablaReservaServicio() + " SET ID_PRODUCTO = ? WHERE ID_PRODUCTO = ?");
		Query q2 = pm.newQuery(SQL, "UPDATE " + ph.darTablaReservaServicio() + " SET ID_SERVICIO = ? WHERE ID_SERVICIO = ?") ;
		
		q2.setParameters(viejo, nuevo);
		q.setParameters(viejo, nuevo);
		q.executeUnique();
		q2.executeUnique();
	}
}
