package uniandes.isis2304.sistrans.hotelAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.sistrans.hotelAndes.negocio.Habitacion;
import uniandes.isis2304.sistrans.hotelAndes.negocio.Servicio;

public class SQLServicio {
	
	public PersistenciaHotelAndes ph;
	
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra ac� para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaHotelAndes.SQL;
	
	public SQLServicio(PersistenciaHotelAndes ph) {
		this.ph = ph;
	}
	
	public long adicionarServicio (PersistenceManager pm, long pHotel, int pId, String pApertura, String pCierre, int pCosto, int pTipo, int pCapacidad, String pDescripcion) 
	{
		Query q = pm.newQuery(ph.SQL, "INSERT INTO " + ph.darTablaServicio() + "(ID_HOTEL, ID_SERVICIO, HORA_APERTURA, HORA_CIERRE, COSTO, ID_TIPO, CAPACIDAD, DESCRIPCION) values (?, ?, ?, ?, ?, ?, ?, ?)");
		q.setParameters(pHotel, pId, pApertura, pCierre, pCosto, pTipo, pCapacidad, pDescripcion);
		return (long) q.executeUnique();
	}
	
	public List<Servicio> darServiciosDisponiblesPorHotelDeCiertoTipoID(PersistenceManager pm, long idHotel, long idTipo, int capacidad) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + ph.darTablaServicio() + " WHERE  ID_HOTEL = ? AND ID_TIPO = ? AND CAPACIDAD <= ? AND ID_MANTENIMIENTO IS NULL");
		q.setResultClass(Servicio.class);
		q.setParameters(idHotel, idTipo, capacidad);
		return (List<Servicio>) q.executeList();
	}
	
	public void sacarDeMantenimiento(PersistenceManager pm, long idMantenimiento)
	{
		Query q = pm.newQuery(SQL, "UPDATE " + ph.darTablaServicio() + "SET ID_MANTENIMIENTO = NULL WHERE ID_MANTENIMIENTO = ?");
		q.setParameters(idMantenimiento);
		q.executeUnique();
	}
	
	
}