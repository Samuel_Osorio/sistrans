package uniandes.isis2304.sistrans.hotelAndes.persistencia;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;

import java.util.List;

import javax.jdo.JDODataStoreException;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Transaction;

import org.apache.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import uniandes.isis2304.sistrans.hotelAndes.negocio.Cliente;
import uniandes.isis2304.sistrans.hotelAndes.negocio.Consumo;
import uniandes.isis2304.sistrans.hotelAndes.negocio.Convencion;
import uniandes.isis2304.sistrans.hotelAndes.negocio.Habitacion;
import uniandes.isis2304.sistrans.hotelAndes.negocio.Mantenimiento;
import uniandes.isis2304.sistrans.hotelAndes.negocio.Reserva;
import uniandes.isis2304.sistrans.hotelAndes.negocio.ReservaServicio;
import uniandes.isis2304.sistrans.hotelAndes.negocio.Servicio;
import uniandes.isis2304.sistrans.hotelAndes.negocio.TipoDeServicio;
import uniandes.isis2304.sistrans.hotelAndes.negocio.Usuario;




public class PersistenciaHotelAndes {
	public final static String SQL = "javax.jdo.query.SQL";
	
	private static Logger log = Logger.getLogger(PersistenciaHotelAndes.class.getName());
	
	
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * Atributo privado que es el �nico objeto de la clase - Patr�n SINGLETON
	 */
	private static PersistenciaHotelAndes instance;

	/**
	 * F�brica de Manejadores de persistencia, para el manejo correcto de las transacciones
	 */
	private PersistenceManagerFactory pmf;

	private List<String> tablas;

	private SQLConsumo sqlConsumo;

	private SQLFacturacion sqlFacturacion;

	private SQLHabitacion sqlHabitacion;

	private SQLHotel sqlHotel;

	private SQLInventario sqlInventario;

	private SQLPlanConsumo sqlPlanConsumo;

	private SQLReserva sqlReserva;

	private SQLReservaServicio sqlReservaServicio;

	private SQLServicio sqlHotelTipoServicio;

	private SQLUsuario sqlUsuario;

	private SQLCliente sqlCliente;
	
	private SQLConvencion sqlConvencion;
	
	private SQLMantenimiento sqlMantenimiento;
	
	private SQLTipo  sqlTipoServicio;

	/**
	 * Atributo para el acceso a las sentencias SQL propias a PersistenciaParranderos
	 */
	private SQLUtil sqlUtil;

	private PersistenciaHotelAndes()
	{
		pmf = JDOHelper.getPersistenceManagerFactory("HotelAndes");		
		crearClasesSQL ();

		tablas = new LinkedList<String> ();
		tablas.add ("HotelAndes_sequence");
		tablas.add("CONSUMO");
		tablas.add("FACTURACION");
		tablas.add("HABITACION");
		tablas.add("HOTEL");
		tablas.add("INVENTARIO");
		tablas.add("PLANCONSUMO");
		tablas.add("RESERVA");
		tablas.add("RESERVASERVICIO");
		tablas.add("HOTEL_TIPOS_SERVICIOS");
		tablas.add("USUARIO");
		tablas.add("CLIENTE");
	}

	/**
	 * Constructor privado, que recibe los nombres de las tablas en un objeto Json - Patr�n SINGLETON
	 * @param tableConfig - Objeto Json que contiene los nombres de las tablas y de la unidad de persistencia a manejar
	 */
	private PersistenciaHotelAndes (JsonObject tableConfig)
	{
		crearClasesSQL ();
		tablas = leerNombresTablas (tableConfig);

		String unidadPersistencia = tableConfig.get ("unidadPersistencia").getAsString ();
		log.trace ("Accediendo unidad de persistencia: " + unidadPersistencia);
		pmf = JDOHelper.getPersistenceManagerFactory (unidadPersistencia);
	}

	/**
	 * @return Retorna el �nico objeto PersistenciaParranderos existente - Patr�n SINGLETON
	 */
	public static PersistenciaHotelAndes getInstance ()
	{
		if (instance == null)
		{
			instance = new PersistenciaHotelAndes ();
		}
		return instance;
	}
	
	public static PersistenciaHotelAndes getInstance (JsonObject tableConfig)
	{
		if (instance == null)
		{
			instance = new PersistenciaHotelAndes (tableConfig);
		}
		return instance;
	}
	
	/**
	 * Cierra la conexi�n con la base de datos
	 */
	public void cerrarUnidadPersistencia ()
	{
		pmf.close ();
		instance = null;
	}
	
	private void crearClasesSQL() {
		sqlConsumo = new SQLConsumo(this);
		sqlFacturacion = new SQLFacturacion(this);
		sqlHabitacion = new SQLHabitacion(this);
		sqlHotel = new SQLHotel(this);
		sqlInventario = new SQLInventario(this);
		sqlPlanConsumo = new SQLPlanConsumo(this);
		sqlReserva = new SQLReserva(this);
		sqlReservaServicio = new SQLReservaServicio(this);
		sqlHotelTipoServicio = new SQLServicio(this);
		sqlUsuario = new SQLUsuario(this);
		sqlUtil = new SQLUtil(this);
		sqlCliente = new SQLCliente(this);
		sqlConvencion = new SQLConvencion(this);
		sqlMantenimiento = new SQLMantenimiento(this);
	}
	
	/**
	 * @return La cadena de caracteres con el nombre del secuenciador de parranderos
	 */
	public String darSeqHotelAndes ()
	{
		return tablas.get (0);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de TipoBebida de parranderos
	 */
	public String darTablaConsumo ()
	{
		return tablas.get (2);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Bebida de parranderos
	 */
	public String darTablaFacturacion ()
	{
		return tablas.get (4);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Bar de parranderos
	 */
	public String darTablaHabitacion ()
	{
		return tablas.get (5);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Bebedor de parranderos
	 */
	public String darTablaHotel ()
	{
		return tablas.get (6);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Gustan de parranderos
	 */
	public String darTablaInventario ()
	{
		return tablas.get (8);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Sirven de parranderos
	 */
	public String darTablaPlanConsumo ()
	{
		return tablas.get (3);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Visitan de parranderos
	 */
	public String darTablaReserva ()
	{
		return tablas.get (11);
	}
	
	public String darTablaReservaServicio()
	{
		return tablas.get(12);
	}
	
	public String darTablaServicio() {
		return tablas.get(13);
	}
	
	public String darTablaUsuario() {
		return tablas.get(14);
	}
	
	public String darTablaCliente() {
		return tablas.get(1);
	}
	
	public String darTablaConvencion() {
		return tablas.get(16);
	}
	
	public String darTablaMantenimiento() {
		return tablas.get(10);
	}
	
	
	
	/**
	 * Transacci�n para el generador de secuencia de Parranderos
	 * Adiciona entradas al log de la aplicaci�n
	 * @return El siguiente n�mero del secuenciador de Parranderos
	 */
	private long nextval ()
	{
        long resp = sqlUtil.nextval (pmf.getPersistenceManager());
        log.trace ("Generando secuencia: " + resp);
        return resp;
    }
	
	/**
	 * Extrae el mensaje de la exception JDODataStoreException embebido en la Exception e, que da el detalle espec�fico del problema encontrado
	 * @param e - La excepci�n que ocurrio
	 * @return El mensaje de la excepci�n JDO
	 */
	private String darDetalleException(Exception e) 
	{
		String resp = "";
		if (e.getClass().getName().equals("javax.jdo.JDODataStoreException"))
		{
			JDODataStoreException je = (javax.jdo.JDODataStoreException) e;
			return je.getNestedExceptions() [0].getMessage();
		}
		return resp;
	}
	
	/**
	 * Genera una lista con los nombres de las tablas de la base de datos
	 * @param tableConfig - El objeto Json con los nombres de las tablas
	 * @return La lista con los nombres del secuenciador y de las tablas
	 */
	private List <String> leerNombresTablas (JsonObject tableConfig)
	{
		JsonArray nombres = tableConfig.getAsJsonArray("tablas") ;

		List <String> resp = new LinkedList <String> ();
		for (JsonElement nom : nombres)
		{
			resp.add (nom.getAsString ());
		}
		
		return resp;
	}
	
	/**
	 * M�todo que inserta, de manera transaccional, una tupla en la tabla BEBEDOR
	 * Adiciona entradas al log de la aplicaci�n
	 * @param nombre - El nombre del bebedor
	 * @param ciudad - La ciudad del bebedor
	 * @param presupuesto - El presupuesto del bebedor (ALTO, MEDIO, BAJO)
	 * @return El objeto BEBEDOR adicionado. null si ocurre alguna Excepci�n
	 */
	public Usuario adicionarUsuario(int numDoc, String tipoDocumento, String nombre, String correo, int idHotel, String tipo) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long tuplasInsertadas = sqlUsuario.adicionarUsuario(pmf.getPersistenceManager(), numDoc, tipoDocumento, nombre, correo, idHotel, tipo);
			tx.commit();

			log.trace ("Inserci�n de bebedor: " + nombre + ": " + tuplasInsertadas + " tuplas insertadas");

			return new Usuario (numDoc, correo, nombre, tipo, tipoDocumento, idHotel);
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	public Cliente adicionarCliente(int numDoc, String tipoDocumento, String nombre, String correo, int idHotel, int idHabitacion) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long tuplasInsertadas = sqlCliente.adicionarCliente(pmf.getPersistenceManager(), numDoc, tipoDocumento, nombre, correo, idHotel, idHabitacion);
			tx.commit();

			log.trace ("Inserci�n de bebedor: " + nombre + ": " + tuplasInsertadas + " tuplas insertadas");

			return new Cliente (numDoc, tipoDocumento, correo, nombre, idHotel, idHabitacion);
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	public Usuario darUsuarioPorId (int idUsuario) 
	{
		return (Usuario) sqlUsuario.darUsuarioPorId (pmf.getPersistenceManager(), idUsuario);
	}

	public Servicio adicionarServicio(int pHotel, int pId, String pApertura, String pCierre, int pCosto, int pTipo, int pCapacidad, String pDescripcion, int pIdUsuario) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		Usuario admin = darUsuarioPorId(pIdUsuario);
		if(admin.getTipoUsuario().equals("Administrador")) {
			try
			{
				tx.begin();
				long tuplasInsertadas = sqlHotelTipoServicio.adicionarServicio(pmf.getPersistenceManager(), pHotel, pId, pApertura, pCierre, pCosto, pTipo, pCapacidad, pDescripcion);
				tx.commit();

				log.trace ("Inserci�n de bebedor: " + pTipo + ": " + tuplasInsertadas + " tuplas insertadas");

				return new Servicio (pId, pApertura, pCierre, pDescripcion, pCapacidad, pHotel, pTipo, pCosto);
			}
			catch (Exception e)
			{
				//        	e.printStackTrace();
				log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
				return null;
			}
			finally
			{
				if (tx.isActive())
				{
					tx.rollback();
				}
				pm.close();
			}
		}
		else {
			return null;
		}
	}
	
	public Reserva adicionarReserva(String pId, String pEntrada, String pSalida, int pNum, String pTipo, int pDoc) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long tuplasInsertadas = sqlReserva.agregarReserva(pmf.getPersistenceManager(), pId, pEntrada, pSalida, pNum, pTipo, pDoc);
			tx.commit();

			log.trace ("Inserci�n de bebedor: " + pDoc + ": " + tuplasInsertadas + " tuplas insertadas");

			return new Reserva (pId, pEntrada, pSalida, pNum, pTipo, pDoc);
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	
	public Consumo adicionarConsumo(int pId, String pInicio, String pFin, int pCantidad, char pCargo, int pProducto, int pHabitacion, int pHotel) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long tuplasInsertadas = sqlConsumo.agregarReserva(pmf.getPersistenceManager(), pId, pInicio, pFin, pCantidad, pCargo, pProducto, pHabitacion, pHotel);
			tx.commit();

			log.trace ("Inserci�n de consumo: " + pHabitacion + ": " + tuplasInsertadas + " tuplas insertadas");

			return new Consumo (pId, pInicio, pFin, pCantidad, pCargo, pProducto, pHabitacion, pHotel);
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	
	
	public Habitacion adicionarHabitacion(long id, long idHotel, String tipo, char reservada, int capacidad, int costo, String idReserva)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try {
			tx.begin();
			long tuplasInsertadas = sqlHabitacion.adicionarHabitacion(pm, id, idHotel, tipo, reservada, capacidad, costo, idReserva);
			tx.commit();
			

			log.trace ("Inserci�n de consumo: " + idHotel + ": " + tuplasInsertadas + " tuplas insertadas");
			
			return new Habitacion(id, idHotel, tipo, reservada, capacidad, costo, idReserva);
		}
		catch(Exception e)
		{
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
		
	}
	
	
	/*
	 * 
	 */
	public void checkIn(long idCliente, String tipoDocumento, String idReserva) throws Exception
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		
		if(sqlReserva.darReserva(pm, idReserva) == null)
		{
			throw new Exception("La reserva con el id: " + idReserva + "no existe");
		}
		
		try {
			
			tx.begin();
			List<Habitacion> habitaciones =  sqlHabitacion.darHabitacionesPorNumReserva(pm, idReserva);
			tx.commit();
			
			for(int i =0; i<habitaciones.size(); i++)
			{
				Habitacion hab = habitaciones.get(i);
				sqlHabitacion.ocuparUnaHabitacion(pm, hab.getId_habitacion(), hab.getHotel().getId(), idReserva);
				sqlCliente.registrarHabitacion(pm, hab.getId_habitacion(), hab.getHotel().getId(), idCliente, tipoDocumento);
			}
			

		}
		catch(Exception e){
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	
	/**
	 * 
	 * @param pm
	 */
	public void checkOut(String idReserva)
	{
		
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		
		try {
		
			tx.begin();
			List<Habitacion> habitaciones = sqlHabitacion.darHabitacionesPorNumReserva(pm, idReserva);
			tx.commit();
			
			for(int i =0; i<habitaciones.size(); i++)
			{
				Habitacion hab = habitaciones.get(i);
				tx.begin();
				sqlHabitacion.desocuparUnaHabitacion(pm, hab.getId_habitacion(), hab.getHotel().getId());
				tx.commit();
			}
			
			tx.begin();
			long numDocCliente = sqlReserva.darDocumentoClienteReservo(pm, idReserva);
			String tipoDocCliente = sqlReserva.darTipoDocumentoClienteReservo(pm, idReserva);
			tx.commit();
			
			tx.begin();
			sqlCliente.sacarDeHabitacion(pm, numDocCliente, tipoDocCliente);
			tx.commit();
		}
		catch(Exception e){
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	
	public ReservaServicio adicionarReservaDeServicio(long  pId, long idHotel, long idServicio, long idTipo, long idHab, Date fechaInicial, Date fechaFinal) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long tuplasInsertadas = sqlReservaServicio.agregarReserva(pm, pId, idHotel, idServicio, idTipo, idHab, fechaInicial, fechaFinal);
			tx.commit();

			log.trace ("Inserci�n de bebedor: " + pId + ": " + tuplasInsertadas + " tuplas insertadas");

			return new ReservaServicio(pId, idHotel, idServicio, idTipo, idHab, fechaInicial, fechaFinal);
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	
	/**
	 * 
	 * @param habitacionesAReservar tupla de String e Integer que define el tipo de Habitacion y la Cantidad que se quiere reservar
	 * @param serviciosAReservar
	 */
	public void RF12registrarAlojamientoYServiciosConvencion(long idHotel, long idConvencion, List<Object[]> habitacionesAReservar, List<String> serviciosAReservar)
	{
		List<Habitacion> habitacionesDisponiblesParaReserva = new ArrayList();
		List<Servicio> serviciosDisponiblesParaReserva = new ArrayList();
		
		List<Object> reservaTotal = new ArrayList<>();
		
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		
		try {
		tx.begin();
		int capacidad = sqlCliente.clientesPertenecenAConvencion(pm, idConvencion).size();
		tx.commit();
		
		tx.begin();
		for(int i = 0; i< serviciosAReservar.size(); i++)
		{
			TipoDeServicio x = sqlTipoServicio.darIdSegunNombresServicios(pm, serviciosAReservar.get(i));
			serviciosDisponiblesParaReserva.add((Servicio) sqlHotelTipoServicio.darServiciosDisponiblesPorHotelDeCiertoTipoID(pm, idHotel, x.getId(), capacidad));
		}
		tx.commit();
		
		
		//el object es una tupla entre tipo de habitacion que quieren ayuda y haceno'
		tx.begin();
		for(int i = 0; i < habitacionesAReservar.size(); i++)
		{
			List<Habitacion> habiitaciones = sqlHabitacion.darHabitacionesDisponiblesPorHotelDeCiertoTipo(pm, idHotel, (String) habitacionesAReservar.get(i)[0]);
			if(habiitaciones.size()> (Integer) habitacionesAReservar.get(i)[1])
			{
				for(int j = 0; i < habiitaciones.size(); i++)
				{
					habitacionesDisponiblesParaReserva.add(habiitaciones.get(j));
				}
			}
		}
		tx.commit();
		}
		catch(Exception e)
		{
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	
	/**
	 * 
	 * @param idConvencion
	 */
	public void RF14registrarFinDeConvencion(long idConvencion)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try
		{
			tx.begin();
			List<Cliente> clientesConvencion = sqlCliente.clientesPertenecenAConvencion(pm, idConvencion);
			tx.commit();
			
			tx.begin();
			Convencion conv = sqlConvencion.darConvencionPorID(pm, idConvencion);
			tx.commit();
			
			tx.begin();
			List<Habitacion> habitaciones = sqlHabitacion.darHabitacionesPorNumReserva(pm, conv.getIdReservaHotel());
			tx.commit();
			
			for(int i =0; i<habitaciones.size(); i++)
			{
				Habitacion hab = habitaciones.get(i);
				tx.begin();
				sqlHabitacion.desocuparUnaHabitacion(pm, hab.getId_habitacion(), hab.getHotel().getId());
				tx.commit();
			}
 			
			for(int i = 0; i<clientesConvencion.size(); i++)
			{
				Cliente x = clientesConvencion.get(i);
				tx.begin();
				sqlCliente.sacarDeHabitacion(pm, x.getNumDocumento(), x.getTipoDocumento());
				tx.commit();
			}
		}
		catch(Exception e)
		{
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	
	public void RF16registrarFinDeMantenimientos()
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		
		try {
		tx.begin();
		List<Mantenimiento> man = sqlMantenimiento.darMantenimientosExpirados(pm);
		tx.commit();
		
		for(int i =0; i < man.size(); i++)
		{
			Mantenimiento m = man.get(i);
			sqlHabitacion.sacarDeMantenimiento(pm, m.getId());
			sqlHotelTipoServicio.sacarDeMantenimiento(pm, m.getId());
		}
		}
		catch(Exception e)
		{
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	
}
