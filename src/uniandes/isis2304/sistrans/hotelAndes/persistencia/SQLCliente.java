package uniandes.isis2304.sistrans.hotelAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;

import javax.jdo.Query;

import uniandes.isis2304.sistrans.hotelAndes.negocio.Cliente;

public class SQLCliente {
	public PersistenciaHotelAndes ph;
	
	public SQLCliente(PersistenciaHotelAndes ph) {
		this.ph = ph;
	}
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un BEBEDOR a la base de datos de Parranderos
	 * @param pm - El manejador de persistencia
	 * @param idBebedor - El identificador del bebedor
	 * @param nombre - El nombre del bebedor
	 * @param ciudad - La ciudad del bebedor
	 * @param presupuesto - El presupuesto del bebedor (ALTO, MEDIO, BAJO)
	 * @return EL n�mero de tuplas insertadas
	 */
	public long adicionarCliente (PersistenceManager pm, int numDoc, String tipoDocumento, String nombre, String correo, int idHotel, int idHabitacion) 
	{
		Query q = pm.newQuery(ph.SQL, "INSERT INTO " + ph.darTablaCliente() + "(NUM_DOCUMENTO, TIPO_DOCUMENTO, CORREO, NOMBRE, ID_HABITACION, ID_HOTEL) values (?, ?, ?, ?, ?, ?)");
		q.setParameters(numDoc, tipoDocumento, correo, nombre, idHabitacion, idHotel);
		return (long) q.executeUnique();
	}
	
	
	/**
	 * 
	 */
	public void registrarHabitacion(PersistenceManager pm, long idHabitacion, long idHotel, long idCliente, String tipoDocumento)
	{
		Query q = pm.newQuery(ph.SQL, "UPDATE" + ph.darTablaCliente() + "SET ID_HABITACION = ?, ID_HOTEL = ?" + "WHERE NUM_DOCUMENTO = ? AND TIPO_DOCUMENTO = ?");
		q.setParameters(idHabitacion, idHotel, idCliente, tipoDocumento);
		q.executeUnique();
	}
	
	
	/**
	 * 
	 */
	public void sacarDeHabitacion(PersistenceManager pm, long idCliente, String tipoDocumento)
	{
		Query q = pm.newQuery(ph.SQL, "UPDATE" + ph.darTablaCliente() + "SET ID_HABITACION = NULL, ID_HOTEL = NULL" + "WHERE NUM_DOCUMENTO = ? AND TIPO_DOCUMENTO = ?");
		q.setParameters(idCliente, tipoDocumento);
		q.executeUnique();
	}
	
	/**
	 * 
	 * @param pm
	 * @param idConvencion
	 * @return
	 */
	public List<Cliente> clientesPertenecenAConvencion(PersistenceManager pm, long idConvencion)
	{
		Query q = pm.newQuery(ph.SQL, "SELECT * FROM " + ph.darTablaCliente() + "WHERE ID_CONVENCION = ?");
		q.setParameters(idConvencion);
		return (List<Cliente>) q.executeList();
	}
	
}

