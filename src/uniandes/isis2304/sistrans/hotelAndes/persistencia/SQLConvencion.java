package uniandes.isis2304.sistrans.hotelAndes.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import uniandes.isis2304.sistrans.hotelAndes.negocio.Convencion;

public class SQLConvencion {

	
	public PersistenciaHotelAndes ph;
	
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra ac� para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaHotelAndes.SQL;
	
	
	public SQLConvencion(PersistenciaHotelAndes pp)
	{
		ph = pp;
	}
	
	
	public Convencion darConvencionPorID(PersistenceManager pm, long idConvencion)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + ph + "WHERE ID = ?");
		q.setParameters(idConvencion);
		return (Convencion) q.executeUnique();
	}
	
	
}
