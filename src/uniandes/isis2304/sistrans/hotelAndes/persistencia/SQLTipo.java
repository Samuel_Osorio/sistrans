package uniandes.isis2304.sistrans.hotelAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.sistrans.hotelAndes.negocio.TipoDeServicio;

public class SQLTipo {

	
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * 
	 */
	private PersistenciaHotelAndes ph;
	
	
	/* ****************************************************************
	 * 			M�todos
	 *****************************************************************/

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	private final static String SQL = PersistenciaHotelAndes.SQL;
	/**
	 * El manejador de persistencia general de la aplicaci�n
	 */
	private PersistenciaHotelAndes pp;
	
	/* ****************************************************************
	 * 			M�todos
	 *****************************************************************/


	
	
	public TipoDeServicio darIdSegunNombresServicios(PersistenceManager pm, String nombre)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM HOTEL_TIPOS_SERVICIOS WHERE NOMBRE = ?" );
		q.setParameters(nombre);
		return (TipoDeServicio) q.executeUnique();
	}
	
}
