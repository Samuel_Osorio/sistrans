package uniandes.isis2304.sistrans.hotelAndes.negocio;

import org.apache.log4j.Logger;

import com.google.gson.JsonObject;

import uniandes.isis2304.sistrans.hotelAndes.persistencia.PersistenciaHotelAndes;

public class HotelAndes {
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Logger para escribir la traza de la ejecuci�n
	 */
	private static Logger log = Logger.getLogger(HotelAndes.class.getName());

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia
	 */
	private PersistenciaHotelAndes ph;

	/* ****************************************************************
	 * 			M�todos
	 *****************************************************************/
	/**
	 * El constructor por defecto
	 */
	public HotelAndes ()
	{
		ph = PersistenciaHotelAndes.getInstance ();
	}

	/**
	 * El constructor qye recibe los nombres de las tablas en tableConfig
	 * @param tableConfig - Objeto Json con los nombres de las tablas y de la unidad de persistencia
	 */
	public HotelAndes (JsonObject tableConfig)
	{
		ph = PersistenciaHotelAndes.getInstance (tableConfig);
	}

	/**
	 * Cierra la conexi�n con la base de datos (Unidad de persistencia)
	 */
	public void cerrarUnidadPersistencia ()
	{
		ph.cerrarUnidadPersistencia ();
	}

	/* ****************************************************************
	 * 			M�todos para manejar los TIPOS DE BEBIDA
	 *****************************************************************/
	/**
	 * Adiciona de manera persistente un tipo de bebida 
	 * Adiciona entradas al log de la aplicaci�n
	 * @param nombre - El nombre del tipo de bebida
	 * @return El objeto TipoBebida adicionado. null si ocurre alguna Excepci�n
	 */
	public Usuario adicionarUsuario (int numDoc, String tipoDocumento, String nombre, String correo, int idHotel, String tipo)
	{
		log.info ("Adicionando Tipo de bebida: " + nombre);
		Usuario tipoBebida = ph.adicionarUsuario (numDoc, tipoDocumento, nombre, correo, idHotel, tipo);		
		log.info ("Adicionando Tipo de bebida: " + tipoBebida);
		return tipoBebida;
	}

	public Cliente adicionarCliente(int numDoc, String tipoDocumento, String nombre, String correo, int idHotel, int idHabitacion) {
		log.info ("Adicionando Tipo de bebida: " + nombre);
		Cliente tipoBebida = ph.adicionarCliente (numDoc, tipoDocumento, nombre, correo, idHotel, idHabitacion);		
		log.info ("Adicionando Tipo de bebida: " + tipoBebida);
		return tipoBebida;
	}
	
	public Servicio adicionarServicio(int pHotel, int pId, String pApertura, String pCierre, int pCosto, int pTipo, int pCapacidad, String pDescripcion, int pIdUsuario) {
		log.info ("Adicionando Tipo de bebida: " + pId);
		Servicio tipoBebida = ph.adicionarServicio (pHotel, pId, pApertura, pCierre, pCosto, pTipo, pCapacidad, pDescripcion, pIdUsuario);		
		log.info ("Adicionando Tipo de bebida: " + tipoBebida);
		return tipoBebida;
	}
	
	public Reserva adicionarReserva(String pId, String pEntrada, String pSalida, int pNum, String pTipo, int pDoc) {
		log.info ("Adicionando Tipo de bebida: " + pId);
		Reserva tipoBebida = ph.adicionarReserva (pId, pEntrada, pSalida, pNum, pTipo, pDoc);		
		log.info ("Adicionando Tipo de bebida: " + tipoBebida);
		return tipoBebida;
	}
	
	public Consumo adicionarConsumo(int pId, String pInicio, String pFin, int pCantidad, char pCargo, int pProducto, int pHabitacion, int pHotel) {
		log.info ("Adicionando Tipo de bebida: " + pId);
		Consumo tipoBebida = ph.adicionarConsumo (pId, pInicio, pFin, pCantidad, pCargo, pProducto, pHabitacion, pHotel);		
		log.info ("Adicionando Tipo de bebida: " + tipoBebida);
		return tipoBebida;
	}
	
	
	

}
