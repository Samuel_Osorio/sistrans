package uniandes.isis2304.sistrans.hotelAndes.negocio;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Convencion {

	/**
	 * el id de la convencion
	 */
	private long id;
	
	/**
	 * la fecha de inicio de la convencion
	 */
	private String fechaInicio;
	
	/**
	 * la fecha final de la convencion
	 */
	private String fechaFin;
	
	/**
	 * los clientes que pertenencen a la convencion
	 */
	private List<Cliente> clientes;
	
	/**
	 * las reservas de servicios que agende la convencion
	 */
	private List<ReservaServicio> serviciosReservados;
	
	/**
	 * 
	 */
	private String idReservaHotel;

	
	public String getIdReservaHotel() {
		return idReservaHotel;
	}

	public void setIdReservaHotel(String idReservaHotel) {
		this.idReservaHotel = idReservaHotel;
	}

	public Convencion() {
		this.id = 0;
		this.fechaInicio = "";
		this.fechaFin = "";
		this.clientes = new ArrayList();
		this.serviciosReservados = new ArrayList();
		this.idReservaHotel = "";
	}
	
	public Convencion(long id, String fechaInicio, String fechaFin, String idReserva ) {
		this.id = id;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		if(idReserva != null && idReserva != "")
		{
			this.idReservaHotel = idReserva;
		}
	}
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	public List<ReservaServicio> getServiciosReservados() {
		return serviciosReservados;
	}

	public void setServiciosReservados(List<ReservaServicio> serviciosReservados) {
		this.serviciosReservados = serviciosReservados;
	} 
	
	
	
	
}
