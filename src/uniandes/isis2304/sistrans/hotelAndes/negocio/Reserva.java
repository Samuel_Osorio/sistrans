package uniandes.isis2304.sistrans.hotelAndes.negocio;
import java.util.HashSet;
import java.util.Set;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */

public class Reserva
{
	
	private String idReserva;
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private String fecha_entrada;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private String fecha_salida;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private int numPersonas;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public String tipoDoc;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public int docCliente;

	
	public Reserva(){
		idReserva = "";
		fecha_entrada = "";
		fecha_salida = "";
		numPersonas = 0;
		tipoDoc = "";
		docCliente = 0;
	}
	
	public Reserva(String pId, String pEntrada, String pSalida, int pNum, String pTipo, int pDoc)
	{
		idReserva = pId;
		fecha_entrada = pEntrada;
		fecha_salida = pSalida;
		numPersonas = pNum;
		tipoDoc = pTipo;
		docCliente = pDoc;
	}

	public String getIdReserva() {
		return idReserva;
	}

	public void setIdReserva(String idReserva) {
		this.idReserva = idReserva;
	}

	public String getFecha_entrada() {
		return fecha_entrada;
	}

	public void setFecha_entrada(String fecha_entrada) {
		this.fecha_entrada = fecha_entrada;
	}

	public String getFecha_salida() {
		return fecha_salida;
	}

	public void setFecha_salida(String fecha_salida) {
		this.fecha_salida = fecha_salida;
	}

	public int getNumPersonas() {
		return numPersonas;
	}

	public void setNumPersonas(int numPersonas) {
		this.numPersonas = numPersonas;
	}

	public String getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public int getDocCliente() {
		return docCliente;
	}

	public void setDocCliente(int docCliente) {
		this.docCliente = docCliente;
	}
	
	
}
