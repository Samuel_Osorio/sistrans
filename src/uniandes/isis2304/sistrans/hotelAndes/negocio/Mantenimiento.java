package uniandes.isis2304.sistrans.hotelAndes.negocio;

public class Mantenimiento {

	/**
	 * 
	 */
	private long id;
	
	/**
	 * 
	 */
	private String descripcion;
	
	/**
	 * 
	 */
	private String fechaInicio;
	
	
	/**
	 * 
	 */
	private String fechaFin;

	
	
	/**
	 * Constructor que llena los datos por default
	 */
	public Mantenimiento()
	{
		id = 0;
		descripcion = "";
		fechaInicio = "";
		fechaFin = "";
	}
	
	/**
	 * Constructor de la instancia a partir de parametros de creacion especificados 
	 * @param pId el id del mantenimiento
	 * @param pDes la descripcion o el motivo del mantenimiento 
	 * @param fecha_in la fecha de inicio del mantenimiento
	 * @param fecha_fi la fecha de finalizacion del mantenimiento
	 */
	public Mantenimiento(long pId, String pDes, String fechaI, String fechaF)
	{
		id = pId;
		descripcion = pDes;
		fechaInicio = fechaI;
		fechaFin = fechaF;
	}
	
	/**
	 * Metodo para obtener el id
	 * @return el id del mantenimiento
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Metodo para cambiar el id
	 * @param id el id nuevo 
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Metodo para obtener la descripcion del mantenimiento 
	 * @return la descripcion del motivo del mantenimiento
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Metodo para cambiar la descripcion del mantenimiento
	 * @param descripcion la nueva descripcion del mantenimiento 
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Metodo que retorna la fecha inicial del mantenimiento
	 * @return la fecha inicial del mantenimiento
	 */
	public String getFechaInicio() {
		return fechaInicio;
	}
	
	/**
	 * Metodo que cambia la fecha inicial de un mantenimiento 
	 * @param fecha_inicio la nueva fecha inicial de un mantenimiento
	 */
	public void setFechaInicio(String fecha_inicio) {
		this.fechaInicio = fecha_inicio;
	}
	
	/**
	 * Metodo que retorna la fecha final de un mantenimiento 
	 * @return fecha de finalizacion de un mantenimiento 
	 */
	public String getFechaFin() {
		return fechaFin;
	}
	
	/**
	 * Metodo para cambiar la fecha de finalizacion de un mantenimiento 
	 * @param fecha_fin  la nueva fecha de finalizacion del mantenimiento
	 */
	public void setFechaFin(String fecha_fin) {
		this.fechaFin = fecha_fin;
	} 
	
	
}
