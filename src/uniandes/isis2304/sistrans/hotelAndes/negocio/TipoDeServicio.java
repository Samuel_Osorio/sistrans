package uniandes.isis2304.sistrans.hotelAndes.negocio;

public class TipoDeServicio {

	
	private String nombre; 
	
	private long id;
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public TipoDeServicio() {
		// TODO Auto-generated constructor stub
		nombre = "";
		id = 0;
	}
	
	public TipoDeServicio(long identificador, String nombre)
	{
		this.nombre = nombre;
		this.id = identificador;
	}
}
