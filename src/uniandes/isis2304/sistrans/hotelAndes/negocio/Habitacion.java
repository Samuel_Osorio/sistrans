package uniandes.isis2304.sistrans.hotelAndes.negocio;
import java.util.HashSet;
import java.util.Set;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */

public class Habitacion
{
	
	/**
	 * 
	 */
	public static final String TIPO_SUITE_FAMILIAR = "SUITE FAMILIAR";
	
	/**
	 * 
	 */
	public static final String TIPO_SUITE_PRESIDENCIAL = "SUITE PRESIDENCIAL";
	
	/**
	 * 
	 */
	public static final String TIPO_DOBLE = "DOBLE";
	
	/**
	 * 
	 */
	public static final String TIPO_SENCILLA = "SENCILLA";
	
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private long id_habitacion;

	public long getId_habitacion() {
		return id_habitacion;
	}

	public Hotel getHotel() {
		return hotel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public String tipo_habitacion;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public Hotel hotel;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public Reserva reserva;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public Set<Cliente> cliente;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public Set<Consumo> consumos;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public Set<ReservaServicio> reservaServicio;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 */
	public Habitacion(){
		
	}

	public Habitacion(long id, long idHotel, String tipo, char reservada, int capacidad, int costo, String idReserva) {
		// TODO Auto-generated constructor stub
	
		
		
	}

}

