package uniandes.isis2304.sistrans.hotelAndes.negocio;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */

public class Usuario
{
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private int numDocumento;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private String correo_electronico;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private String nombre;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private String tipoUsuario;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private String tipoDocumento;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public int idHotel;


	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 */
	public Usuario(){
		numDocumento = 0;
		correo_electronico = "";
		nombre = "";
		tipoUsuario = "";
		tipoDocumento = "";
		idHotel = 0;
	}

	public Usuario(int pDoc, String pCorreo, String pNombre, String pTipo, String pTipoDoc, int pId){
		numDocumento = pDoc;
		correo_electronico = pCorreo;
		nombre = pNombre;
		tipoUsuario = pTipo;
		tipoDocumento = pTipoDoc;
		idHotel = pId;
	}

	public int getNumDocumento() {
		return numDocumento;
	}


	public void setNumDocumento(int numDocumento) {
		this.numDocumento = numDocumento;
	}


	public String getCorreo_electronico() {
		return correo_electronico;
	}


	public void setCorreo_electronico(String correo_electronico) {
		this.correo_electronico = correo_electronico;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getTipoUsuario() {
		return tipoUsuario;
	}


	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}


	public String getTipoDocumento() {
		return tipoDocumento;
	}


	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}


	public int getHotel() {
		return idHotel;
	}


	public void setHotel(int hotel) {
		this.idHotel = hotel;
	}

	
}

