package uniandes.isis2304.sistrans.hotelAndes.negocio;

import java.sql.Date;

/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */

public class ReservaServicio
{
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private int id;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private Date fecha_inicio;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	private Date fehca_fin;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public long servicio;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 * @ordered
	 */
	
	public long habitacion;
	
	/**
	 * 
	 */
	public long hotel;
	
	/**
	 * 
	 */
	public long tipo;

	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 */
	public ReservaServicio(){
		super();
	}

	public ReservaServicio(long pId, long idHotel, long idServicio, long idTipo, long idHab, Date fechaInicial,
			Date fechaFinal) {
		
		this.id = (int) pId;
		this.servicio = idServicio;
		this.habitacion = idHab;
		this.tipo = idTipo;
		this.hotel = idHotel;
		this.fecha_inicio = fechaInicial;
		this.fehca_fin = fechaFinal;
	}

}

